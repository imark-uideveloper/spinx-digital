import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout-component';
import { AppHeaderComponent } from './layout/app-header/app-header-component';
import { HomeComponent } from './layout/home/home.component';
import { OurservicesComponent } from './layout/ourservices/ourservices.component';
import { OurservicessingleComponent } from './layout/ourservicessingle/ourservicessingle.component';
import { AboutComponent } from './layout/about/about.component';
import { BlogComponent } from './layout/blog/blog.component';
import { BlogsingleComponent } from './layout/blogsingle/blogsingle.component';
import { PortfolioComponent } from './layout/portfolio/portfolio.component';
import { PortfoliosingleComponent } from './layout/portfoliosingle/portfoliosingle.component';
import { ContactusComponent } from './layout/contactus/contactus.component';
import { AppFooterComponent } from './layout/app-footer/app-footer-component';


import { AppRoutingModule } from './app.routing';

@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, AppRoutingModule, BrowserAnimationsModule ],
  declarations: [
    AppComponent, AppHeaderComponent, AppLayoutComponent, HomeComponent, OurservicesComponent, OurservicessingleComponent, AboutComponent, BlogComponent, BlogsingleComponent, AppFooterComponent, PortfolioComponent,PortfoliosingleComponent, ContactusComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
