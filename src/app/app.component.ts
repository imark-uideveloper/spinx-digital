import { Component } from '@angular/core';
import { fadeAnimation } from './router.animations';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  animations: [fadeAnimation]
})
export class AppComponent  {

 }
