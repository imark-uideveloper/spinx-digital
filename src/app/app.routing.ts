import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AppLayoutComponent } from './layout/app-layout/app-layout-component';
import { HomeComponent } from './layout/home/home.component';
import { OurservicesComponent } from './layout/ourservices/ourservices.component';
import { OurservicessingleComponent } from './layout/ourservicessingle/ourservicessingle.component';
import { AboutComponent } from './layout/about/about.component';
import { PortfolioComponent } from './layout/portfolio/portfolio.component';
import { PortfoliosingleComponent } from './layout/portfoliosingle/portfoliosingle.component';
import { BlogComponent } from './layout/blog/blog.component';
import { BlogsingleComponent } from './layout/blogsingle/blogsingle.component';
import { ContactusComponent } from './layout/contactus/contactus.component';

export const routes: Routes = [
    {
        path: '',
        component: AppLayoutComponent,
        children: [
            { path: '', component: HomeComponent, pathMatch: 'full' },
            { path: 'ourservices', component: OurservicesComponent },
            { path: 'ourservicessingle', component: OurservicessingleComponent },
            { path: 'about', component: AboutComponent },
            { path: 'blog', component: BlogComponent },
            { path: 'blog-single', component: BlogsingleComponent },
            { path: 'portfolio', component: PortfolioComponent },
            { path: 'portfoliosingle', component: PortfoliosingleComponent },
            { path: 'contact-us', component: ContactusComponent }
        ]
    },
];
@NgModule({
    imports: [
      RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
  })
  export class AppRoutingModule {}