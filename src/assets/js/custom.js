(function(jQuery) {
  'use strict';

	jQuery(document).ready(function(){

		// Wow Js
		var wow = new WOW({
			mobile: false // default
		})
		wow.init();

		// Page Scroll
		jQuery('a[href*="#"]:not([href="#"])').click(function () {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = jQuery(this.hash);
				target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					jQuery('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});

		// Portfolio
		jQuery(".filter-button").click(function(){
			jQuery('.filter-button').removeClass('active');
			jQuery(this).addClass('active');
			var value = jQuery(this).attr('data-filter');
			if(value == "all") {
				jQuery('.filter').show('1000');
			}
			else {
				jQuery(".filter").not('.'+value).hide('3000');
				jQuery('.filter').filter('.'+value).show('3000');
			}
		});

		//Header
		jQuery(window).on("load resize scroll", function(e) {
			var Win = jQuery(window).height();
			var Header = jQuery("header").height();
			var Footer = jQuery("footer").height();
			var NHF = Header + Footer;
			jQuery('.main').css('min-height', (Win - NHF));
		});

		//Textarea
		jQuery('.firstCap, textarea').on('keypress', function(event) {
			var jQuerythis = jQuery(this),
			thisVal = jQuerythis.val(),
			FLC = thisVal.slice(0, 1).toUpperCase(),
			con = thisVal.slice(1, thisVal.length);
			jQuery(this).val(FLC + con);
		});

		//Sticky Header
		jQuery(window).scroll(function () {
			var scroll = jQuery(window).scrollTop();
			if (scroll >= 200) {
				jQuery(".header").addClass("animated fadeInDown fixed-header");
			} else {
				jQuery(".header").removeClass("animated fadeInDown fixed-header");
			}
        });


        var top_header_video = $('.banner-wrapper video');
        top_header_video.css({'top':'0'}); // better use CSS

        $(window).scroll(function () {
        var st = $(this).scrollTop();
        top_header_video.css({'top':(st*.5)+'px'});
        });

        var top_header_text = $('.banner-wrapper .banner-content');
        top_header_text.css({'top':'0'}); // better use CSS

        $(window).scroll(function () {
        var st = $(this).scrollTop();
        top_header_text.css({'top':(st+2)+'px'});
        });


	});

	//Page Zoom
	document.documentElement.addEventListener('touchstart', function (event) {
		if (event.touches.length > 1) {
			event.preventDefault();
		}
	}, false);

	//Avoid pinch zoom on iOS
	document.addEventListener('touchmove', function(event) {
		if (event.scale !== 1) {
			event.preventDefault();
		}
    }, false);


})(jQuery)

/*---------------------------------------------------------------------
- Progress Bar Circle
---------------------------------------------------------------------*/

//Note -- I removed the respondCanvas function from the circiful library
/* PROGRESS CIRCLE COMPONENT */

jQuery(function() {
	jQuery('#progress').circliful();
	jQuery('#progress1').circliful();
	jQuery('#progress2').circliful();
});

(function (jQuery) {

    jQuery.fn.circliful = function (options, callback) {

        var settings = jQuery.extend({
            // These are the defaults.
            startdegree: 0,
            fgcolor: "#556b2f",
            bgcolor: "#eee",
            fill: false,
            width: 15,
            dimension: 200,
            fontsize: 15,
            percent: 50,
            animationstep: 1.0,
            iconsize: '20px',
            iconcolor: '#999',
            border: 'default',
            complete: null,
            bordersize: 10
        }, options);

        return this.each(function () {

            var customSettings = ["fgcolor", "bgcolor", "fill", "width", "dimension", "fontsize", "animationstep", "endPercent", "icon", "iconcolor", "iconsize", "border", "startdegree", "bordersize"];

            var customSettingsObj = {};
            var icon = '';
            var endPercent = 0;
            var obj = jQuery(this);
            var fill = false;
            var text, info;

            obj.addClass('circliful');

            checkDataAttributes(obj);

            if (obj.data('text') != undefined) {
                text = obj.data('text');

                if (obj.data('icon') != undefined) {
                    icon = jQuery('<i></i>')
                        .addClass('fa ' + jQuery(this).data('icon'))
                        .css({
                            'color': customSettingsObj.iconcolor,
                            'font-size': customSettingsObj.iconsize
                        });
                }

                if (obj.data('type') != undefined) {
                    type = jQuery(this).data('type');

                    if (type == 'half') {
                        addCircleText(obj, 'circle-text-half', (customSettingsObj.dimension / 1.45));
                    } else {
                        addCircleText(obj, 'circle-text', customSettingsObj.dimension);
                    }
                } else {
                    addCircleText(obj, 'circle-text', customSettingsObj.dimension);
                }
            }

            if (jQuery(this).data("total") != undefined && jQuery(this).data("part") != undefined) {
                var total = jQuery(this).data("total") / 100;

                percent = ((jQuery(this).data("part") / total) / 100).toFixed(3);
                endPercent = (jQuery(this).data("part") / total).toFixed(3)
            } else {
                if (jQuery(this).data("percent") != undefined) {
                    percent = jQuery(this).data("percent") / 100;
                    endPercent = jQuery(this).data("percent")
                } else {
                    percent = settings.percent / 100
                }
            }

            if (jQuery(this).data('info') != undefined) {
                info = jQuery(this).data('info');

                if (jQuery(this).data('type') != undefined) {
                    type = jQuery(this).data('type');

                    if (type == 'half') {
                        addInfoText(obj, 0.9);
                    } else {
                        addInfoText(obj, 1.25);
                    }
                } else {
                    addInfoText(obj, 1.25);
                }
            }

            jQuery(this).width(customSettingsObj.dimension + 'px');

            var canvas = jQuery('<canvas></canvas>').attr({
                width: customSettingsObj.dimension,
                height: customSettingsObj.dimension
            }).appendTo(jQuery(this)).get(0);

            var context = canvas.getContext('2d');
            var container = jQuery(canvas).parent();
            var x = canvas.width / 2;
            var y = canvas.height / 2;
            var degrees = customSettingsObj.percent * 360.0;
            var radians = degrees * (Math.PI / 180);
            var radius = canvas.width / 2.5;
            var startAngle = 2.3 * Math.PI;
            var endAngle = 0;
            var counterClockwise = false;
            var curPerc = customSettingsObj.animationstep === 0.0 ? endPercent : 0.0;
            var curStep = Math.max(customSettingsObj.animationstep, 0.0);
            var circ = Math.PI * 2;
            var quart = Math.PI / 2;
            var type = '';
            var fireCallback = true;
            var additionalAngelPI = (customSettingsObj.startdegree / 180) * Math.PI;

            if (jQuery(this).data('type') != undefined) {
                type = jQuery(this).data('type');

                if (type == 'half') {
                    startAngle = 2.0 * Math.PI;
                    endAngle = 3.13;
                    circ = Math.PI;
                    quart = Math.PI / 0.996;
                }
            }

            /**
             * adds text to circle
             *
             * @param obj
             * @param cssClass
             * @param lineHeight
             */
            function addCircleText(obj, cssClass, lineHeight) {
                jQuery("<span></span>")
                    .appendTo(obj)
                    .addClass(cssClass)
                    .text(text)
                    .prepend(icon)
                    .css({
                        'line-height': lineHeight + 'px',
                        'font-size': customSettingsObj.fontsize + 'px'
                    });
            }

            /**
             * adds info text to circle
             *
             * @param obj
             * @param factor
             */
            function addInfoText(obj, factor) {
                jQuery('<span></span>')
                    .appendTo(obj)
                    .addClass('circle-info-half')
                    .css(
                        'line-height', (customSettingsObj.dimension * factor) + 'px'
                    )
                    .text(info);
            }

            /**
             * checks which data attributes are defined
             * @param obj
             */
            function checkDataAttributes(obj) {
                jQuery.each(customSettings, function (index, attribute) {
                    if (obj.data(attribute) != undefined) {
                        customSettingsObj[attribute] = obj.data(attribute);
                    } else {
                        customSettingsObj[attribute] = jQuery(settings).attr(attribute);
                    }

                    if (attribute == 'fill' && obj.data('fill') != undefined) {
                        fill = true;
                    }
                });
            }

            /**
             * animate foreground circle
             * @param current
             */
            function animate(current) {

                context.clearRect(0, 0, canvas.width, canvas.height);

                context.beginPath();
                context.arc(x, y, radius, endAngle, startAngle, false);

                context.lineWidth = customSettingsObj.bordersize + 1;

                context.strokeStyle = customSettingsObj.bgcolor;
                context.stroke();

                if (fill) {
                    context.fillStyle = customSettingsObj.fill;
                    context.fill();
                }

                context.beginPath();
                context.arc(x, y, radius, -(quart) + additionalAngelPI, ((circ) * current) - quart + additionalAngelPI, false);

                if (customSettingsObj.border == 'outline') {
                    context.lineWidth = customSettingsObj.width + 13;
                } else if (customSettingsObj.border == 'inline') {
                    context.lineWidth = customSettingsObj.width - 13;
                }

                context.strokeStyle = customSettingsObj.fgcolor;
                context.stroke();

                if (curPerc < endPercent) {
                    curPerc += curStep;
                    requestAnimationFrame(function () {
                        animate(Math.min(curPerc, endPercent) / 100);
                    }, obj);
                }

                if (curPerc == endPercent && fireCallback && typeof(options) != "undefined") {
                    if (jQuery.isFunction(options.complete)) {
                        options.complete();

                        fireCallback = false;
                    }
                }
            }

            animate(curPerc / 100);

        });
    };
}(jQuery));
